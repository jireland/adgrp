# NAME
    adgrp - manipulate AD groups from the command line

# SYNOPSIS
     adgrp list [-s -e -c -l -h -d -v] [-u <user-name>] [-p <password>] <group-name> [<group-name>, ...]
     adgrp userlist [-s -e -c -l -h -d -v] [-u <user-name>] [-p <password>] <group-name> [<group-name>, ...]
     adgrp find [-h -e -d -v] [-u <user-name>] [-p <password>] <group-name> [<group-name>, ...]
     adgrp addusr [-n -h -d -v] [-u <user-name>] [-p <password>] <group-name> <user-name> [<user-name>, ...]
     agrrp addgrp [-n -h -d -v] [-u <user-name>] [-p <password>] <user-name> <group-name> [<group-name>, ...]
     adgrp rmusr [-n -h -d -v] [-u <user-name>] [-p <password>] <group-name> <user-name>  [<user-name>, ...]
     adgrp rmgrp [-n -h -d -v] [-u <user-name>] [-p <password>] <user-name> <group-name>  [<group-name>, ...]
     adgrp memberOf [-s -e -c -l -h -d -v] <user-name>, [<user-name>, ...]
     adgrp listOU [-s -e -c -h -d -v] "<OU-path>", ["<OU-path>", ...] 
     adgrp managedBy [-s -e -c -h -d -v] <group-name>  [<group-name>, ...]
     adgrp groupcreate [-d -n] <AD location> <group-name>  [<group-name>, ...]
     adgrp groupdelete [-d -n] <group-name>  [<group-name>, ...]

# DESCRIPTION
    adgrp can manipulate AD groups from the command line, allowing multiple
    groups and users to be processed in a single command. The script
    connects to a UoE AD server storing passing the users password for each
    operation preformed.

    adgrp list - list the members of a UoE AD group.

    adgrp userlist - list the members of a UoE AD group, recursively
    expanding any groups found.

    adgrp find - find the location of an UoE AD group.

    adgrp addusr - add users to a UoE AD group.

    adgrp addgrp - add single user to a list of UoE AD groups.

    adgrp rmusr - delete users from a UoE AD group.

    adgrp rmgrp - delete a single user from a list of UoE AD groups.

    adgrp memberOf - list a users AD group membership.

    adgrp managedBy - list users (or AD group) in a groups Managed-By
    attribute. Not always set.

    adgrp listOU - list the contents of an OU.

    adgrp groupcreate - create new groups.

    adgrp groupdelete - delete groups.

    This command is a wrapper script for adtool : http://gp2x.org/adtool/ by
    Mike Dawson <mike@gp2x.org> and ldapsearch

    You will be prompted to provide your AD password for the user-name held
    in the bash environment variable LOGNAME, or given via the -u option.

    To use these commands, each user requires the file ~/.ldaprc to contain
    the line,

    TLS_REQCERT never

    This file will be created, with this line, automatically if it does not
    exist.

# OPTIONS
    -c  Print output as Comma Seperated Values on a single line - best used
        with -s option.

    -e  Ecsape any printed white space with a back-slash.

    -l  Print output on single line - best used with -s option.

    -d  Debug: Print the adtool command used to interact with UoE AD.

    -n  Do not make any changes. Useful with the -d option.

    -h  Print help.

    -p <password>
        Supply password from the command line

    -s  Print short answer: the CN object, either user-name or group-name,
        not the full AD path.

    -u <user-name>
        Connect to AD as <user-name>

    -v  Print version.

# AUTHOR
    John Ireland - UoE IGMM - Tue 15 Mar 16:19:43 GMT 2016

# SEE ALSO
    adtool(1)

